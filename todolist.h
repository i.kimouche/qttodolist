#ifndef TODOLIST_H
#define TODOLIST_H

#include <QObject>
#include <QVector>

struct ToDoItem
{
    bool done;
    QString desc;
};

class ToDoList : public QObject
{
    Q_OBJECT
public:
    explicit ToDoList(QObject *parent = nullptr);

    QVector<ToDoItem> items() const;

    bool setItemAt(int index, const ToDoItem &item);

signals:
void preItemAppend();
void postItemAppend();
void preItemRemoved(int index);
void postItemRemoved();
void postClear();

public slots:
    void appendItem();
    void removeCompletedItems();
    void clearAll();

private:
    QVector<ToDoItem> mItems;

};

#endif // TODOLIST_H
