import QtQuick 2.9
import QtQuick.Controls 2.2

ApplicationWindow {
    visible: true
    width: 350
    height: 350
    title: qsTr("To Do List")

   ToDoList {
       anchors.centerIn: parent
   }
}
