import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import ToDo 1.0

ColumnLayout {
    Frame {
        Layout.fillWidth: true
        ListView {
            implicitWidth: 250
            implicitHeight: 250
            clip: true
            anchors.fill: parent

            model: ToDoModel {
                list: myList

            }

            delegate: RowLayout {
                width: parent.width
                CheckBox {
                    checked: model.done
                    onClicked: model.done =checked
                }
                TextField {
                    text: model.description
                    onEditingFinished: model.desc = text
                    Layout.fillWidth: true
                }
            }
        }
    }
    RowLayout {
        Button {
            Layout.fillWidth: true;
            text: qsTr("ADD");
            onClicked: myList.appendItem()
        }
        Button {
            Layout.fillWidth: true;
            text: qsTr("CLEAR COMPLETED");
            onClicked: myList.removeCompletedItems()
        }

        Button {
            text: qsTr("CLEAR ALL")
            onClicked: myList.clearAll()
        }
    }
}
